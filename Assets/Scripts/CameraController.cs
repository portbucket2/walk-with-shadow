﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class CameraController : MonoBehaviour
{
    [SerializeField] CinemachineVirtualCamera vCam;
    GameController gameController;
    Transform player;
    void Start()
    {
        gameController = GameController.GetController();
        player = gameController.GetPlayer().transform;
    }

    public void FocusGate(Transform gate)
    {
        vCam.LookAt = gate;
        vCam.Follow = gate;
    }
    public void FocusPlayer()
    {
        vCam.LookAt = player;
        vCam.Follow = player;
    }

}
