﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class LightPivot : MonoBehaviour
{
    [SerializeField] bool inputEnabled = false;
    [SerializeField] float Speed = 0.1f;
    [SerializeField] float targetvalue = 100f;
    [SerializeField] float snapDistance = 3f;
    [SerializeField] Transform DirectionaleLight;
    [SerializeField] Transform torchHolder;


    [Header("Debug : ")]
    [SerializeField] Transform ride;
    //[SerializeField] Transform secondSpotLightHolder;
    //[SerializeField] Transform secondSpotLight;
    Vector3 mouseStart;
    GameController gameController;
    float h = 0;
    bool isRiding = false;
    string currentTrigger;

    void Start()
    {
        gameController = GameController.GetController();
    }

    // Update is called once per frame
    void Update()
    {
        if (!inputEnabled)
            return;

        if (Input.GetMouseButtonDown(0))
        {
            mouseStart = Input.mousePosition;
        }
        if (Input.GetMouseButton(0))
        {
            SetRotationOfLight(Vector3.Distance(mouseStart, Input.mousePosition));
        }
        if (Input.GetMouseButtonUp(0))
        {

        }
    }

    void SetRotationOfLight(float distance)
    {
        if (distance > 0.1f)
        {
            h = Speed * Input.GetAxis("Mouse X");
            transform.eulerAngles += new Vector3(0f,0f, -h * Time.deltaTime * Speed);
            torchHolder.eulerAngles += new Vector3(0f,0f, -h * Time.deltaTime * Speed);
        }
        TargetCheck();
    }

    void TargetCheck()
    {
        Vector3 cc = transform.eulerAngles;
        Vector3 tt = new Vector3(0f,0f, targetvalue);
        if (Vector3.Distance(cc, tt) < snapDistance ) 
        {
            
            inputEnabled = false;
            Snap();
        }
    }
    void Snap()
    {
        transform.DOLocalRotate(new Vector3(0f, 0f, targetvalue), 0.35f).OnComplete(ShadowMatched);
        torchHolder.DOLocalRotate(new Vector3(0f, 0f, targetvalue), 0.35f);
        if (isRiding)
        {
            gameController.GetPlayer().SetRider(ride, currentTrigger);       
        }
    }
    void ShadowMatched()
    {
        //Debug.Log("shadow matched");
        if (!gameController.GetPlayer().isSwinger())
        {
            
            gameController.LevelComplete();
        }
        else
        {
            Debug.Log("swing start");
            gameController.GetPlayer().SwingStart();
        }
       
        
    }
    //public void SetLightAngle(LevelData _levelData)
    //{
    //    LevelData data = _levelData;
    //    float steepAngle = data.startHeightAngle;
    //    float target = data.targetRotationAngle;
    //    float secondRotationAngle = data.secondLightRotationAngle;
    //    Vector2 range = data.startRotationAngle;
        
    //    targetvalue = target;
    //    DirectionaleLight.localEulerAngles = new Vector3(steepAngle, 0f, 0f);
    //    transform.DOLocalRotate(new Vector3(0f, Random.Range(range.x, range.y), 0f), 0.35f);
    //    if (secondRotationAngle > 0)
    //    {
    //        Debug.Log("second: " + secondRotationAngle);
    //        secondSpotLightHolder.gameObject.SetActive(true);
    //        secondSpotLightHolder.DOLocalRotate(new Vector3(0f, secondRotationAngle, 0f), 0f);
    //        float y = data.secondLightHeight;
    //        float z = data.secondLightDistance;
    //        secondSpotLight.localPosition = new Vector3(0f, y, z);
    //    }
    //    else
    //    {
    //        secondSpotLightHolder.gameObject.SetActive(false);
    //    }
    //}
    public void SetLightTarget(LightAngles angles, Transform _cameratarget)
    {
        EnableShadow();
        torchHolder.gameObject.SetActive(true);
        torchHolder.position = _cameratarget.position;

        targetvalue = angles.rotation;
        Vector3 rot = new Vector3(0f, 0f, Random.Range(angles.startRotationAngle.x, angles.startRotationAngle.y));
        transform.DOLocalRotate(rot,0.3f);
        torchHolder.DOLocalRotate(rot,0.3f);
        DirectionaleLight.localEulerAngles = new Vector3(0f, angles.height, 0f);
        isRiding = angles.isRider;
        //Debug.Log("is riding: " + isRiding);
        if (isRiding)
        {
            GameObject gg = Instantiate(angles.isRider);
            ride = gg.transform;
            currentTrigger = angles.trigger;
        }
    }
    public void RandomLightDirection()
    {
        Vector3 rot = new Vector3(0f, 0f, Random.Range(0, 180));
        transform.DOLocalRotate(rot, 0.3f);
        torchHolder.DOLocalRotate(rot, 0.3f);

        torchHolder.gameObject.SetActive(false);
    }
    public void EnableInput()
    {
        inputEnabled = true;
    }
    public void EnableShadow() { DirectionaleLight.gameObject.SetActive(true); }
    public void DisableShadow() { DirectionaleLight.gameObject.SetActive(false); }
}


