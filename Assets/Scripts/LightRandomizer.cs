﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightRandomizer : MonoBehaviour
{
   
    GameController gameController;
    LightPivot lightPivot;
    readonly string PLAYER = "Player";
    void Start()
    {
        gameController = GameController.GetController();
        lightPivot = gameController.GetLightPivote();
    }


    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag(PLAYER))
        {
            lightPivot.RandomLightDirection();
            
        }
        
    }

}
