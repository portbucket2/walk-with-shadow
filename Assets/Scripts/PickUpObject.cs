﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickUpObject : MonoBehaviour
{
    public string animTriggerOnPickUp;
    public bool addSwinger = false;
    public float disappearAfter = -1f;
    PlayerController player;

    readonly string PLAYER = "Player";
    WaitForSeconds WAIT;
    WaitForEndOfFrame END = new WaitForEndOfFrame();
    SpriteRenderer self;
    SpriteRenderer otherSelf;

    private void Start()
    {
        player = GameController.GetController().GetPlayer();
        WAIT = new WaitForSeconds(disappearAfter);
        self = transform.GetComponent<SpriteRenderer>();
        otherSelf = transform.GetChild(0).GetComponent<SpriteRenderer>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag(PLAYER))
        {
            if (addSwinger)
            {
                player.AddSwinger();
            }
            transform.SetParent(player.transform);
            player.PlayCustom(animTriggerOnPickUp);
            player.PickedUp();
            transform.GetChild(0).gameObject.SetActive(true);
            GetComponent<BoxCollider>().enabled = false;
            if (disappearAfter > 0)
            {
                StartCoroutine(DisappearAfter());
            }
        }
    }
    IEnumerator DisappearAfter()
    {
        yield return WAIT;
        player.PlayCustom("walk");
        transform.parent = null;
        float t = 1;
        while (t>0)
        {
            self.color = new Color(self.color.r, self.color.g, self.color.b, t);
            otherSelf.color = new Color(otherSelf.color.r, otherSelf.color.g, otherSelf.color.b, t);
            t -= Time.deltaTime;
            yield return END;
        }
        yield return END;
    }
}
