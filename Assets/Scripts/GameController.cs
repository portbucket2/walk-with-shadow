﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour
{
    public static GameController gameController;

    private void Awake()
    {
        gameController = this;
    }
    public static GameController GetController() { return gameController; }

    [SerializeField] LightPivot lightPivot;
    [SerializeField] PlayerController playerController;
    [SerializeField] SceneController sceneController;
    [SerializeField] CameraController cameraController;
    [SerializeField] UIController uiController;


    public LightPivot GetLightPivote() { return lightPivot; }
    public PlayerController GetPlayer() { return playerController; }
    public SceneController GetSceneController() { return sceneController; }
    public CameraController GetCameraController() { return cameraController; }
    public UIController GetUI() { return uiController; }


    public void LevelComplete()
    {
        sceneController.ShadowMatchComplete();
        //playerController.SwingStart();
    }
}
