﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UIController : MonoBehaviour
{
    [SerializeField] GameObject levelCompletePanel;
    [SerializeField] Button nextLevelButton;

    GameManager gameManager;
    WaitForSeconds WAITONE = new WaitForSeconds(1f);

    void Start()
    {
        gameManager = GameManager.GetManager();

        nextLevelButton.onClick.AddListener(delegate
        {
            gameManager.GotoNextStage();
        });
    }

    public void LevelComplete()
    {
        StartCoroutine(LevelCompleteRoutine());
    }
    IEnumerator LevelCompleteRoutine()
    {
        yield return WAITONE;
        levelCompletePanel.SetActive(true);
    }
}
