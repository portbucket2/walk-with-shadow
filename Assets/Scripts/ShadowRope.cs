﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShadowRope : MonoBehaviour
{
    public Transform startPos;
    public Transform targetPos;
    public LineRenderer ln;
    public float offset = 1f;

    bool isSwinging = false;

    void Start()
    {
        
    }

    void Update()
    {
        if (isSwinging)
        {
            ln.SetPosition(0, startPos.position);
            ln.SetPosition(1, targetPos.position + Vector3.up * offset);
        }
    }

    public void SwingStart() { isSwinging = true; ln.enabled = true; }
    public void SwingEnd() { isSwinging = false; }

}
