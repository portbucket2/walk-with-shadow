﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class PlayerController : MonoBehaviour
{
    
    [SerializeField] Animator spriteAnimator;

    [SerializeField] bool isCheck;
    [SerializeField] float linkDuration = 2f;
    [SerializeField] AnimationCurve curve;
    [SerializeField] ShadowRope rope;
    [SerializeField] FollowCurve swinger;

    [Header("Debug: ")]
    [SerializeField] NavMeshAgent agent;
    [SerializeField] bool MoveAcrossNavMeshesStarted = false;
    [SerializeField] bool linking;
    [SerializeField] float origSpeed;
    [SerializeField] Transform characterTransform;
    [SerializeField] Transform currentTargetPosition;
    [SerializeField] SceneController sceneController;
    [SerializeField] bool isRider = false;
    [SerializeField] bool isRiderComplete = false;
    [SerializeField] bool hasPickedUp = false;
    [SerializeField] Transform ride;

    readonly string IDLE = "idle";
    readonly string WALK = "walk";
    readonly string JUMP = "jump";
    readonly string CELEBRATE = "celebrate";

    string currentTrigger;

    WaitForSeconds ONE = new WaitForSeconds(0.5f);
    WaitForSeconds TWO = new WaitForSeconds(1f);

    GameController gameController;
    void Start()
    {
        gameController = GameController.GetController();
        sceneController = gameController.GetSceneController();
        

        agent = GetComponent<NavMeshAgent>();
        isCheck = false;

        origSpeed = agent.speed;
        linking = false;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void FixedUpdate()
    {
        //if (agent.isOnOffMeshLink && linking == false)
        //{
        //    linking = true;
        //    agent.speed = agent.speed * linkSpeed;
        //}
        //else if (agent.isOnNavMesh && linking == true)
        //{
        //    linking = false;
        //    agent.velocity = Vector3.zero;
        //    agent.speed = origSpeed;
        //}

        if (agent.isOnOffMeshLink && !MoveAcrossNavMeshesStarted)
        {
            StartCoroutine(MoveAcrossNavMeshLink());
            MoveAcrossNavMeshesStarted = true;
        }
        if (isCheck)
        {

            if (pathComplete())
            {
                //agent.SetDestination(transform.position);
                Idle();
                isCheck = false;
                //Debug.Log("chk");
                sceneController.StartMiniGame();
                if (hasPickedUp)
                {
                    DiscurdRide();
                }
                agent.speed = origSpeed;
            }
        }
    }


    IEnumerator MoveAcrossNavMeshLink()
    {
        Jump();
        if (transform.childCount >= 2 && isRiderComplete)
        {
            Debug.Log("child deduction: ");
            //transform.GetChild(1).gameObject.SetActive(false);
            transform.GetChild(1).parent = null;
        }

        OffMeshLinkData data = agent.currentOffMeshLinkData;
        agent.updateRotation = false;

        Vector3 startPos = agent.transform.position;
        Vector3 endPos = data.endPos + Vector3.up * agent.baseOffset;
        float v0 = agent.velocity.magnitude;
        float duration = (endPos - startPos).magnitude / agent.velocity.magnitude;
        float t = 0.0f;
        float tStep = 1.0f / duration;
        float preY = characterTransform.localPosition.y;
        while (t < linkDuration)
        {
            transform.position = Vector3.Lerp(startPos, endPos, t);
            agent.destination = transform.position;
            t += tStep * Time.deltaTime;
            float yy = curve.Evaluate(t);
            //Debug.LogError("yy: " + yy);
            
            characterTransform.localPosition = new Vector3(0f, preY + yy, 0f);
            yield return null;
        }
        transform.position = endPos;
        agent.updateRotation = true;
        agent.CompleteOffMeshLink();
        MoveAcrossNavMeshesStarted = false;
        agent.SetDestination(currentTargetPosition.position);

        if (isRider)
        {
            isRiderComplete = true;
            ride.parent = transform;
            ride.GetChild(0).gameObject.SetActive(true);
            gameController.GetLightPivote().DisableShadow();
            isRider = false;
            Play();
        }else
        {
            isRiderComplete = false;
            Walk();
        }

        StopAllCoroutines();
    }
    protected bool pathComplete()
    {
        if (Vector3.Distance(agent.destination, agent.transform.position) <= agent.stoppingDistance)
        {
            if (!agent.hasPath || agent.velocity.sqrMagnitude == 0f)
            {
                return true;
            }
        }

        return false;
    }
    void Walk() { spriteAnimator.SetTrigger(WALK);  }
    void Jump() { spriteAnimator.SetTrigger(JUMP); }
    void Idle() { spriteAnimator.SetTrigger(IDLE); }
    void Play() { spriteAnimator.SetTrigger(currentTrigger); }
    public void PlayCustom(string _trigger) { spriteAnimator.SetTrigger(_trigger); }
    public void Celebrate() { spriteAnimator.SetTrigger(CELEBRATE); }

    public void PickedUp()
    {
        hasPickedUp = true;
        gameController.GetLightPivote().DisableShadow();
        if (isSwinger())
        {
            BikeJump();
        }
    }
    public void DiscurdRide()
    {
        if (transform.childCount >= 2 )
        {
            Debug.Log("child deduction: ");
            //transform.GetChild(1).gameObject.SetActive(false);
            transform.GetChild(1).parent = null;
            hasPickedUp = false;
        }
    }
    public void Move(Transform _target)
    {
        StartCoroutine(WalkRoutine(_target));
    }
    IEnumerator WalkRoutine(Transform _tt)
    {
        currentTargetPosition = _tt;
        agent.SetDestination(_tt.position);
        spriteAnimator.SetTrigger(WALK);
        yield return ONE;
        isCheck = true;
        
    }

    public void SetRider(Transform _ride, string _trigger)
    {
        isRider = true;
        ride = _ride;
        currentTrigger = _trigger;
    }
    public bool isSwinger() { return swinger; }
    public void SwingStart() {
        swinger.enabled = true;
        agent.enabled = false;
        swinger.StartRoute();
        gameController.GetCameraController().FocusPlayer();
        Jump();
        gameController.GetLightPivote().DisableShadow();
        if (rope)
            rope.SwingStart();
    }
    public void SwingEnd() {

        swinger.enabled = false;
        agent.enabled = true;
        agent.SetDestination(transform.position);
        if (rope)
            rope.SwingEnd();
        rope = null;
        swinger = null;
        if (hasPickedUp)
        {
            agent.SetDestination(currentTargetPosition.position);
        }
        else
        {
            Walk();
            gameController.LevelComplete();
        }
        
    }
    public void BikeJump()
    {
        agent.speed = 30f;
        swinger.enabled = true;
        agent.enabled = false;
        swinger.StartRoute();
        gameController.GetCameraController().FocusPlayer();
        
        gameController.GetLightPivote().DisableShadow();

    }
    public void AddSwinger()
    {
        swinger = transform.GetComponent<FollowCurve>();
    }
}
