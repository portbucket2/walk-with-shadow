﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneController : MonoBehaviour
{
    [SerializeField] ParticleSystem confettiParticle;
    [SerializeField] Transform lastGate;
    [SerializeField] GameDataSheet data;
    [SerializeField] List<Transform> gates;
    [SerializeField] List<Transform> cameraTargets;
    


    [Header("Debug :")]
    [SerializeField] bool isFinalgate = false;
    [SerializeField] int gateCount = 0;
    [SerializeField] int currentLevel = 0;
    [SerializeField] PlayerController player;
    [SerializeField] LightPivot lightPivot;
    [SerializeField] List<LightAngles> angles;

    GameController gameController;
    GameManager gameManager;
    AnalyticsController analytics;

    void Start()
    {
        gameManager = GameManager.GetManager();
        if (gameManager)
        {
            currentLevel = gameManager.GetlevelCount();
        }
        gameController = GameController.GetController();
        analytics = AnalyticsController.GetController();
        if (analytics)
        {
            analytics.LevelStarted();
        }


        player = gameController.GetPlayer();
        lightPivot = gameController.GetLightPivote();
        angles = new List<LightAngles>();
        //currentLevel = jnjkasnd
        angles = data.levelDatas[currentLevel].lightAngles;
         
        Invoke("NextGate", 0.5f);
        
    }
    void NextGate()
    {
        if (gateCount < gates.Count)
        {
            player.Move(gates[gateCount]);
        }
        else
        {
            player.Move(lastGate);
            isFinalgate = true;
        }
        
        gameController.GetCameraController().FocusPlayer();
    }
    public void ShadowMatchComplete()
    {
        Debug.Log("next gate");
        gateCount++;
        NextGate();
    }   

    public void StartMiniGame()
    {
        // set camera target
        if (!isFinalgate)
        {
            lightPivot.EnableInput();
            lightPivot.SetLightTarget(angles[gateCount], cameraTargets[gateCount]);
            gameController.GetCameraController().FocusGate(cameraTargets[gateCount]);
        }
        else
        {
            confettiParticle.Play();
            gameController.GetPlayer().Celebrate();
            Debug.Log("level complete!!");
            gameController.GetUI().LevelComplete();
            if (analytics)
            {
                analytics.LevelCompleted();
            }
        }

    }
}
