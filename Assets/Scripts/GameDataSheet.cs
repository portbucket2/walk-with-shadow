﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/GameDataSheet", order = 1)]
public class GameDataSheet : ScriptableObject
{
    public string detail;

    public List<LevelData> levelDatas;

    
}

[System.Serializable]
public struct LevelData
{
    public Color sceneThemeColor;
    public Color shadowColor;

    public List<LightAngles> lightAngles;

    //public Texture shadowArt;
    //[Header("First Light")]
    //public bool isLightOn;
    //public float startHeightAngle;
    //public float targetRotationAngle;
    //public Vector2 startRotationAngle;

    //[Header ("Second Light")]
    //public float secondLightRotationAngle;
    //public float secondLightHeight;
    //public float secondLightDistance;
    //public Vector3 position;
}

[System.Serializable]
public struct LightAngles
{
    public GameObject isRider;
    public string trigger;
    public float rotation;
    public float height;
    public Vector2 startRotationAngle;
}
